package controllables;

import controllers.Light;

public class Lightbulb implements Light {

	public Lightbulb(){
		
	}
	
	public void on(){
		System.out.print("Lightbulb on\n");
	}
	
	public void off(){
		System.out.print("Lightbulb off\n");
	}
}
