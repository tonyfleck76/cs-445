package controllers;

public interface Light {

	public void on();
	public void off();
	
}
