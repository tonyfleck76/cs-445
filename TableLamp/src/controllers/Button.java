package controllers;

public class Button {

	private Light light;
	
	public Button(Light light){
		this.light = light;
	}
	public void switchOn(){
		light.on();
		System.out.print("Button switched to ON\n");
	}
	
	public void switchOff(){
		light.off();
		System.out.print("Button switched to OFF\n");
	}
}
