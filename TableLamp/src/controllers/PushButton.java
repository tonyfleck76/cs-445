package controllers;

public class PushButton {

	private Light light;
	private boolean isOn;
	
	public PushButton(Light light){
		this.light = light;
		isOn = false;
	}
	
	public void pushButton(){
		if (isOn){
			light.off();
			isOn = false;
			System.out.print("Button switched to OFF\n");
		}
		else{
			light.on();
			isOn = true;
			System.out.print("Button switched to ON\n");
		}
	}

	public boolean isOn() {
		return isOn;
	}
	
}
