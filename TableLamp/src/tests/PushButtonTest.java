package tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controllables.Lightbulb;
import controllers.PushButton;

public class PushButtonTest {
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}
	
	@Test
	public void PushButton_Is_Off_When_Created(){
		PushButton push = new PushButton(new Lightbulb());
		assertEquals(false, push.isOn());
	}
	
	@Test
	public void PushButton_Is_On_After_One_Push(){
		PushButton push = new PushButton(new Lightbulb());
		push.pushButton();
		assertEquals(true, push.isOn());
	}
	
	@Test
	public void PushButton_Is_Off_After_Two_Pushes(){
		PushButton push = new PushButton(new Lightbulb());
		push.pushButton();
		push.pushButton();
		assertEquals(false, push.isOn());
	}
	
	@Test
	public void Output_Is_Correct_After_One_Push(){
		PushButton push = new PushButton(new Lightbulb());
		push.pushButton();
		assertEquals("Lightbulb on\nButton switched to ON\n", outContent.toString());
	}
	
	@Test
	public void Output_Is_Correct_After_Two_Pushes(){
		PushButton push = new PushButton(new Lightbulb());
		push.pushButton();
		push.pushButton();
		assertEquals("Lightbulb on\nButton switched to ON\nLightbulb off\nButton switched to OFF\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}
}
