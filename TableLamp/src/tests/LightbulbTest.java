package tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controllables.Lightbulb;

public class LightbulbTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}
	
	@Test
	public void Switch_On_Prints_Correct_Message(){
		Lightbulb lightbulb = new Lightbulb();
		lightbulb.on();
		assertEquals("Lightbulb on\n", outContent.toString());
	}
	
	@Test
	public void Switch_Off_Prints_Correct_Message(){
		Lightbulb lightbulb = new Lightbulb();
		lightbulb.off();
		assertEquals("Lightbulb off\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}
}
