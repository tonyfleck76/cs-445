package tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controllers.Button;
import controllers.Light;

public class ButtonTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}
	
	@Test
	public void Switch_On_Prints_Correct_Message(){
		Button button = new Button(new Light(){

			@Override
			public void on() {
			}

			@Override
			public void off() {
			}
			
		});
		
		button.switchOn();
		
		assertEquals("Button switched to ON\n", outContent.toString());
	}
	
	@Test
	public void Switch_Off_Prints_Correct_Message(){
		Button button = new Button(new Light(){

			@Override
			public void on() {
			}

			@Override
			public void off() {
			}
			
		});
		button.switchOff();
		assertEquals("Button switched to OFF\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}
}
