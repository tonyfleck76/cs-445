package main;

import controllables.Lightbulb;
import controllers.PushButton;

public class PushLamp {

	public static void main(String[] args) {
		PushButton push = new PushButton(new Lightbulb());
		
		push.pushButton();
		push.pushButton();
	}

}
