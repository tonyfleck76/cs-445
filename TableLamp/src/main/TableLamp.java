package main;

import controllables.Lightbulb;
import controllers.Button;

public class TableLamp {

	public static void main(String[] args) {
		Button button = new Button(new Lightbulb());
		
		button.switchOn();
		button.switchOff();
	}

}
