package hw1.classes;

public class Thing {

	private String name;
	
	public Thing(String name){
		this.name = name;
	}
	
	public String toString(){
		return name + " " + getClass().getSimpleName();
	}
}
