package hw1.classes;

public class Ant extends Creature {
	
	public Ant(String name){
		super(name);
	}

	@Override
	public void eat(Thing thing) {
		// STUB
	}

	@Override
	public void move() {
		System.out.print(this + " is crawling around.\n");
	}

	@Override
	public void whatDidYouEat() {
		// STUB
	}

}
