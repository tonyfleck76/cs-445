package hw1.classes;

public class Bat extends Creature implements Flyer {
	
	public Bat(String name){
		super(name);
	}

	@Override
	public void fly() {
		System.out.print(this + " is swooping around in the dark!\n");
	}

	@Override
	public void eat(Thing thing) {
		if (thing instanceof Creature){
			System.out.print(this + " has just eaten a " + thing + "\n");
			setInStomach(thing);
		}
		else if (thing instanceof Thing){
			System.out.print(this + " won't eat a " + thing + "\n");
		}
	}

	@Override
	public void move() {
		this.fly();
	}

	@Override
	public void whatDidYouEat() {
		System.out.print(this + " has eaten a " + getInStomach() + "\n");
	}

}
