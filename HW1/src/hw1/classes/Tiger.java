package hw1.classes;

public class Tiger extends Creature {

	public Tiger(String name) {
		super(name);
	}

	@Override
	public void eat(Thing thing) {
		// STUB
	}

	@Override
	public void move() {
		System.out.print(this + " has just pounced.\n");
	}

	@Override
	public void whatDidYouEat() {
		// STUB
	}
	
}
