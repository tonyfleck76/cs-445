package hw1.classes;

public abstract class Creature extends Thing {

	private Thing inStomach;
	
	public Creature(String name) {
		super(name);
	}

	public abstract void eat(Thing thing);
	
	public abstract void move();
	
	public abstract void whatDidYouEat();

	public Thing getInStomach() {
		return inStomach;
	}

	public void setInStomach(Thing inStomach) {
		this.inStomach = inStomach;
	}
	
}
