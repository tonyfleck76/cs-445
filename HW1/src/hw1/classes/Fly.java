package hw1.classes;

public class Fly extends Creature implements Flyer {
	
	public Fly(String name){
		super(name);
	}

	@Override
	public void fly() {
		System.out.print(this + " is buzzing around in flight.\n");
	}

	@Override
	public void eat(Thing thing) {
		if (thing instanceof Creature){
			System.out.print(this + " won't eat a " + thing + "\n");
		}
		else if (thing instanceof Thing){
			System.out.print(this + " has just eaten a " + thing + "\n");
			setInStomach(thing);
		}
	}

	@Override
	public void move() {
		this.fly();
	}

	@Override
	public void whatDidYouEat() {
		System.out.print(this + " has eaten a " + getInStomach() + "\n");
	}

}
