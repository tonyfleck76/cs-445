package hw1.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hw1.classes.Ant;

public class AntTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}
	
	@Test
	public void when_created_stomach_is_null() {
		Ant ant = new Ant("Dave Foley");
		assertEquals(null, ant.getInStomach());
	}
	
	@Test
	public void tiger_moves_correctly(){
		Ant ant = new Ant("Dave Foley");
		
		ant.move();
		
		assertEquals("Dave Foley Ant is crawling around.\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}

}
