package hw1.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hw1.classes.Ant;
import hw1.classes.Fly;
import hw1.classes.Thing;

public class FlyTest {
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@Test
	public void when_created_stomach_is_null() {
		Fly fly = new Fly("Fred");
		
		assertEquals(null, fly.getInStomach());
	}
	
	@Test
	public void fly_will_not_eat_creature(){
		Fly fly = new Fly("Fred");
		Ant ant = new Ant("Woody Allen");
		
		fly.eat(ant);
		
		assertEquals(null, fly.getInStomach());
	}

	@Test
	public void fly_will_eat_thing(){
		Fly fly = new Fly("Fred");
		Thing thing = new Thing("Apple");
		
		fly.eat(thing);
		
		assertEquals(thing, fly.getInStomach());
	}
	
	@Test
	public void bat_moves_correctly(){
		Fly fly = new Fly("Fred");
		fly.move();
		assertEquals("Fred Fly is buzzing around in flight.\n", outContent.toString());
	}
	
	@Test
	public void fly_prints_what_it_ate(){
		Fly fly = new Fly("Fred");
		Thing thing = new Thing("Apple");
		
		fly.eat(thing);
		
		outContent.reset();
		
		fly.whatDidYouEat();
		
		assertEquals("Fred Fly has eaten a Apple Thing\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}

}
