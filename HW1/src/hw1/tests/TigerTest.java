package hw1.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hw1.classes.Tiger;

public class TigerTest {
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@Test
	public void when_created_stomach_is_null() {
		Tiger tiger = new Tiger("Tony");
		assertEquals(null, tiger.getInStomach());
	}
	
	@Test
	public void tiger_moves_correctly(){
		Tiger tiger = new Tiger("Tony");
		
		tiger.move();
		
		assertEquals("Tony Tiger has just pounced.\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}

}
