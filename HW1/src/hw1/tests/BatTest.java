package hw1.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import hw1.classes.Bat;
import hw1.classes.Fly;
import hw1.classes.Thing;

public class BatTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	
	@Before
	public void setUpStreams(){
		System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}
	
	@Test
	public void when_created_stomach_is_null() {
		Bat bat = new Bat("Benny");
		
		assertEquals(null, bat.getInStomach());
	}
	
	@Test
	public void bat_will_eat_creature(){
		Bat bat = new Bat("Benny");
		Fly fly = new Fly("Fred");
		
		bat.eat(fly);
		
		assertEquals(fly, bat.getInStomach());
	}

	@Test
	public void bat_will_not_eat_thing(){
		Bat bat = new Bat("Benny");
		Thing thing = new Thing("Apple");
		
		bat.eat(thing);
		
		assertEquals(null, bat.getInStomach());
	}
	
	@Test
	public void bat_moves_correctly(){
		Bat bat = new Bat("Benny");
		bat.move();
		assertEquals("Benny Bat is swooping around in the dark!\n", outContent.toString());
	}
	
	@Test
	public void fly_prints_what_it_ate(){
		Bat bat = new Bat("Benny");
		Fly fly = new Fly("Fred");
		
		bat.eat(fly);
		
		outContent.reset();
		
		bat.whatDidYouEat();
		
		assertEquals("Benny Bat has eaten a Fred Fly\n", outContent.toString());
	}
	
	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}
}
