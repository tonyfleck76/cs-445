package hw1.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import hw1.classes.Thing;

public class ThingTest {

	@Test
	public void returns_correct_name_string(){
		Thing t = new Thing("Bob");
		assertEquals("Bob Thing", t.toString());
	}

}
