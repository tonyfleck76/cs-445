package hw1.tests;

import hw1.classes.Ant;
import hw1.classes.Bat;
import hw1.classes.Creature;
import hw1.classes.Fly;
import hw1.classes.Thing;
import hw1.classes.Tiger;

public class TestCreature {

	public static final int CREATURE_COUNT = 6;
	public static final int THING_COUNT = 10;
	
	public static void main(String args[]){
		Thing things[] = new Thing[THING_COUNT];
		things[0] = new Thing("Banana");
		things[1] = new Thing("Tree");
		things[2] = new Tiger("Tony");
		things[3] = new Tiger("Tim");
		things[4] = new Bat("Bob");
		things[5] = new Thing("Steve");
		things[6] = new Ant("Woody Allen");
		things[7] = new Thing("Ball");
		things[8] = new Fly("Steve");
		things[9] = new Ant("Dave Foley");
		
		Creature creatures[] = new Creature[CREATURE_COUNT];
		creatures[0] = (Creature) things[2];
		creatures[1] = (Creature) things[3];
		creatures[2] = (Creature) things[4];
		creatures[3] = (Creature) things[6];
		creatures[4] = (Creature) things[8];
		creatures[5] = (Creature) things[9];
		
		System.out.println("Things:\n");
		
		for (Thing thing : things){
			System.out.println(thing);
		}
		
		System.out.println("\nCreatures:\n");
		
		for (Creature creature : creatures){
			System.out.println(creature);
			creature.move();
			creature.whatDidYouEat();
			creature.eat(things[0]);
			creature.whatDidYouEat();
			creature.eat(creatures[0]);
			creature.whatDidYouEat();
		}
	}
}
