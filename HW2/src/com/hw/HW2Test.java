package com.hw;

public class HW2Test {

	public static void main(String[] args) {
		ImprovedRandom rand = new ImprovedRandom();
		
		int random;
		for (int i = 0; i < 10; i++){
			random = rand.randRange(0,  50);
			System.out.println(random);
		}
		
		ImprovedStringTokenizer tok = new ImprovedStringTokenizer("This class is easy");
		
		String[] tokens = tok.getAllTokens();
		
		for (int i = 0; i < tokens.length; i++){
			System.out.println(tokens[i]);
		}
	}

}
