package com.hw;

import java.util.StringTokenizer;

public class ImprovedStringTokenizer extends StringTokenizer {

	public ImprovedStringTokenizer(String str){
		super(str);
	}
	
	public ImprovedStringTokenizer(String str, String delim){
		super(str, delim);
	}
	
	public ImprovedStringTokenizer(String str, String delim, boolean returnDelims){
		super(str, delim, returnDelims);
	}
	
	public String[] getAllTokens(){
		String[] tokens = new String[super.countTokens()];
		int count = 0;
		
		while(super.hasMoreTokens()){
			tokens[count] = super.nextToken();
			count++;
		}
		
		return tokens;
	}
}
