package com.hw;

import java.util.Random;

public class ImprovedRandom extends Random {

	public ImprovedRandom(){
		super();
	}
	
	public ImprovedRandom(Long seed){
		super(seed);
	}
	
	public int randRange(int min, int max){
		return super.nextInt(max-min)+min;
	}
}
