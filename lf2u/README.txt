Lf2u Project Statistics
* Lines of Code: 1426
* Lines of Code in Unit Tests: 1071
* Unit Test Coverage: 87%
* Cyclomatic Complexity: 1.091 is the average. The maximum is 8.
Building the project:
* Install Java
o Sudo apt-get openjdk-8-jdk
* Install maven
o Sudo apt-get maven
* Cd into the project folder and run the following commands:
o Mvn test
* The code coverage results will be under �target/jacoco-ut/index.html�
o Mvn spring-boot:run
The hardest part of the project:
	I definitely had lots of trouble managing my time properly on this project. I also Found it very difficult to test certain operations when it came to my interactors, especially once I introduced the Lf2u singleton class. Since the tests can run in any order, many tests had to be rewritten to try and allow the tests to work properly since accounts were created and changed many times.
