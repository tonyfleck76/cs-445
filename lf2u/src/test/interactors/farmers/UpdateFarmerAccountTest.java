package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.UpdateFarmerAccountTransaction;

public class UpdateFarmerAccountTest {

	@Test
	public void Transaction_Updates_Account(){
		FarmInfo farmInfo = new FarmInfo("UpdateFarmerTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		UpdateFarmerAccountTransaction transaction = new UpdateFarmerAccountTransaction(new FarmInfo("UpdatedFarmerTest", "100 Test Dr", "000-000-0000", "test.com"), 
					 																	new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), zips);
		transaction.completeTransaction(Integer.parseInt(fid));
		assertEquals("UpdatedFarmerTest", Lf2u.getInstace().findFarmerAccountById(Integer.parseInt(fid)).getFarmInfo().getName());
	}
}
