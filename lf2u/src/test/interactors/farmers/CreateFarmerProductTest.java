package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.exceptions.GenericProductNotFoundException;
import lf2u.interactors.farmers.CreateFarmerProductTransaction;

public class CreateFarmerProductTest {

	@Test
	public void Transaction_Returns_Fspid_String(){
		FarmInfo farmInfo = new FarmInfo("Test", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		
		Lf2u.getInstace().getCatalog().addGenericProduct("Potatoes");
		Lf2u.getInstace().getCatalog().addGenericProduct("Carrots");
		
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		CreateFarmerProductTransaction transaction = new CreateFarmerProductTransaction(1, "Note", "10/01", "11/01", 0.5, "lbs", "image.jpg");
		assertEquals("1", transaction.completeTransaction(Integer.parseInt(fid)));
	}
	
	@Test(expected = GenericProductNotFoundException.class)
	public void Transaction_Fails_If_Gpid_Does_Not_Exist(){
		FarmInfo farmInfo = new FarmInfo("Test", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		CreateFarmerProductTransaction transaction = new CreateFarmerProductTransaction(999, "Note", "10/01", "11/01", 0.5, "lbs", "image.jpg");
	}
}
