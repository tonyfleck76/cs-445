package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.UpdateFarmerDeliveryChargeTransaction;

public class UpdateFarmerDeliveryChargeTest {

	@Test
	public void DeliveryCharge_Updates(){
		FarmInfo farmInfo = new FarmInfo("UpdateFarmerTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		UpdateFarmerDeliveryChargeTransaction transaction = new UpdateFarmerDeliveryChargeTransaction(5.0);
		transaction.completeTransaction(Integer.parseInt(fid));
		assertEquals(5.0, Lf2u.getInstace().findFarmerAccountById(Integer.parseInt(fid)).getDeliveryCharge(), 0);
	}
}
