package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.FarmStore;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.Product;
import lf2u.entities.farmers.ProductInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.GetFarmerProductDetailsTransaction;

public class GetFarmerProductDetailsTest {

	@Test
	public void Correct_Product_Is_Returned(){
		FarmInfo farmInfo = new FarmInfo("GetFarmerProductDetailsTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		Lf2u.getInstace().getCatalog().addGenericProduct("Potatoes");
		Lf2u.getInstace().getCatalog().addGenericProduct("Carrots");
		
		Farmer farm = Lf2u.getInstace().findFarmerAccountById(Integer.parseInt(fid));
		FarmStore store = farm.getFarmStore();
		store.addProductToStore(new ProductInfo("Potatoes", "Note", null, null, null, fid, 0));
		GetFarmerProductDetailsTransaction transaction = new GetFarmerProductDetailsTransaction(Integer.parseInt(fid), 1);
		Product product = transaction.completeTransaction();
		assertEquals("Potatoes", product.getName());
	}
}
