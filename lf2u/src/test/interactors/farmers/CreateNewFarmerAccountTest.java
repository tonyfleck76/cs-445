package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.CreateNewFarmerAccountTransaction;

public class CreateNewFarmerAccountTest {

	@Test
	public void Transaction_Returns_Correct_String(){
		FarmInfo farmInfo = new FarmInfo("CreateTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		
		CreateNewFarmerAccountTransaction transaction = new CreateNewFarmerAccountTransaction(farmInfo, personalInfo, zips);
		String fid = transaction.completeTransaction();
		Farmer newFarm = Lf2u.getInstace().findFarmerAccountById(Integer.parseInt(fid));
		assertEquals("CreateTest", newFarm.getFarmInfo().getName());
	}
}
