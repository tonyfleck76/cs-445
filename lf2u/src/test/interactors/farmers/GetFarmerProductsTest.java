package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.FarmStore;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.ProductInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.GetFarmerProductsTransaction;

public class GetFarmerProductsTest {

	@Test
	public void Transaction_Has_Correct_Product_List(){
		FarmInfo farmInfo = new FarmInfo("GetFarmerProductDetailsTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		Lf2u.getInstace().getCatalog().addGenericProduct("Potatoes");
		Lf2u.getInstace().getCatalog().addGenericProduct("Carrots");
		
		Farmer farm = Lf2u.getInstace().findFarmerAccountById(Integer.parseInt(fid));
		FarmStore store = farm.getFarmStore();
		store.addProductToStore(new ProductInfo("Potatoes", "Note", null, null, null, fid, 0));
		
		GetFarmerProductsTransaction transaction = new GetFarmerProductsTransaction(Integer.parseInt(fid));
		assertEquals(1, transaction.completeTransaction().size());
	}
}
