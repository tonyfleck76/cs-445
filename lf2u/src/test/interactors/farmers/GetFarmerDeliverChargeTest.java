package test.interactors.farmers;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.GetFarmerDeliveryChargeTransaction;

public class GetFarmerDeliverChargeTest {

	@Test
	public void Correct_Charge_Is_Returned(){
		FarmInfo farmInfo = new FarmInfo("GetFarmersTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		GetFarmerDeliveryChargeTransaction transaction = new GetFarmerDeliveryChargeTransaction(Integer.parseInt(fid));
		assertEquals(0, transaction.completeTransaction(), 0);
	}
}
