package test.interactors.farmers;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.ViewFarmersResponse;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.farmers.GetFarmerAccountsTransaction;

public class GetFarmerAccountsTest {

	@Test
	public void Correct_Farms_Are_In_Transaction_Without_Zip(){
		FarmInfo farmInfo = new FarmInfo("GetFarmersTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		GetFarmerAccountsTransaction transaction = new GetFarmerAccountsTransaction();
		ArrayList<ViewFarmersResponse> farmers = transaction.completeTransaction();
		boolean exists = false;
		
		for (ViewFarmersResponse farmer : farmers){
			if (farmer.getName().equals("GetFarmersTest")){
				exists = true;
			}
		}
		
		assertEquals(true, exists);
	}
	
	@Test
	public void Correct_Farms_Are_In_Transaction_With_Zip(){
		FarmInfo farmInfo = new FarmInfo("GetFarmersZipTest", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("99999");
		ArrayList<String> zips2 = new ArrayList<String>();
		zips.add("60000");
		
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips2);
		
		GetFarmerAccountsTransaction transaction = new GetFarmerAccountsTransaction();
		ArrayList<ViewFarmersResponse> farmers = transaction.completeTransaction("99999");
		assertEquals(1, farmers.size());
	}
}
