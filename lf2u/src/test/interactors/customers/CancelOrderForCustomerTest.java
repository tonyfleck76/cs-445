package test.interactors.customers;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.customers.Customer;
import lf2u.entities.customers.Order;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.interactors.customers.CancelOrderForCustomerTransaction;

public class CancelOrderForCustomerTest {

	@Test
	public void Order_Is_Canceled(){
		String cid = Lf2u.getInstace().createCustomerAccount(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), "100 fake street", "60000");
		Customer customer = Lf2u.getInstace().findCustomerAccountById(Integer.parseInt(cid));
		
		FarmInfo farmInfo = new FarmInfo("Test", "100 Test Dr", "000-000-0000", "test.com");
		ContactInfo personalInfo = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, zips);
		
		Lf2u.getInstace().getCatalog().addGenericProduct("Potatoes");
		Lf2u.getInstace().getCatalog().addGenericProduct("Carrots");
		
		customer.addOrder(new Order(1, Integer.parseInt(fid), "Note"));
		
		CancelOrderForCustomerTransaction transaction = new CancelOrderForCustomerTransaction(Integer.parseInt(cid), 1);
		transaction.completeTransaction();
		
		assertEquals("canceled", customer.getOrderById(1).getStatus());
	}
}
