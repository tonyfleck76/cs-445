package test.interactors.customers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.interactors.customers.CreateCustomerAccountTransaction;

public class CreateCustomerAccountTest {

	@Test
	public void Customer_Is_Created(){
		CreateCustomerAccountTransaction transaction = new CreateCustomerAccountTransaction("John Smith", "jsmith@example.com", "000-000-0000", "1000 Fake Street", "60000");
		String cid = transaction.completeTransaction();
		assertEquals("John Smith", Lf2u.getInstace().findCustomerAccountById(Integer.parseInt(cid)).getName());
	}
}
