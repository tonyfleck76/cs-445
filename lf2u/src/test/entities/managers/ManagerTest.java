package test.entities.managers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lf2u.entities.managers.Manager;
import lf2u.entities.wrappers.ContactInfo;

public class ManagerTest {

	@Test
	public void ID_Is_Set_Correctly(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals(0, manager.getMid());
	}
	
	@Test
	public void ID_String_Is_Correct(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals("0", manager.getMidString());
	}

	@Test
	public void CreatedBy_Is_Set_Correctly(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals("Test", manager.getCreatedBy());
	}
	
	@Test
	public void CreateDate_Is_Set_Correctly(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals("20161015", manager.getCreateDate());
	}
	
	@Test
	public void Name_Is_Empty_When_Created(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals(null, manager.getName());
	}
	
	@Test
	public void Email_Is_Empty_When_Created(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals(null, manager.getEmail());
	}
	
	@Test
	public void Phone_Is_Empty_When_Created(){
		Manager manager = new Manager(0, "Test", "20161015");
		assertEquals(null, manager.getPhone());
	}
	
	@Test
	public void SetContactInfo_Updates_Name(){
		Manager manager = new Manager(0, "Test", "20161015");
		manager.setContactInfo(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"));
		assertEquals("John Smith", manager.getName());
	}
	
	@Test
	public void SetContactInfo_Updates_Email(){
		Manager manager = new Manager(0, "Test", "20161015");
		manager.setContactInfo(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"));
		assertEquals("jsmith@example.com", manager.getEmail());
	}
	
	@Test
	public void SetContactInfo_Updates_Phone(){
		Manager manager = new Manager(0, "Test", "20161015");
		manager.setContactInfo(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"));
		assertEquals("000-000-0000", manager.getPhone());
	}
}
