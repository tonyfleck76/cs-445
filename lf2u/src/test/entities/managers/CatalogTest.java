package test.entities.managers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.managers.Catalog;
import lf2u.exceptions.GenericProductNotFoundException;

public class CatalogTest {

	@Test
	public void GenericProducts_Is_Empty_When_Created(){
		Catalog catalog = new Catalog();
		assertEquals(0, catalog.getGenericProducts().size());
	}
	
	@Test
	public void Product_Can_Be_Added(){
		Catalog catalog = new Catalog();
		String gcpid = catalog.addGenericProduct("Potatoes");
		assertEquals("1", gcpid);
	}
	
	@Test
	public void GenericProduct_Can_Update(){
		Catalog catalog = new Catalog();
		String gcpid = catalog.addGenericProduct("Potatoes");
		catalog.updateGenericProduct(1, "Fancy Potatoes");
		assertEquals("Fancy Potatoes", catalog.getGenericProducts().get(0).getName());
	}
	
	@Test(expected = GenericProductNotFoundException.class)
	public void Update_Fails_If_Gcpid_Does_Not_Exist(){
		Catalog catalog = new Catalog();
		catalog.updateGenericProduct(1, "Test");
	}
	
	@Test(expected = GenericProductNotFoundException.class)
	public void Update_Fails_If_Gcpid_Less_Than_Zero(){
		Catalog catalog = new Catalog();
		catalog.updateGenericProduct(-11, "Test");
	}
}
