package test.entities.managers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.managers.GenericProduct;

public class GenericProductTest {

	@Test
	public void Gcpid_Is_Correct(){
		GenericProduct product = new GenericProduct(0, "Potatoes");
		assertEquals(0, product.getGcpid());
	}
	
	@Test
	public void Name_Is_Correct(){
		GenericProduct product = new GenericProduct(0, "Potatoes");
		assertEquals("Potatoes", product.getName());
	}
	
	@Test
	public void Name_Can_Be_Changed(){
		GenericProduct product = new GenericProduct(0, "Potatoes");
		product.setName("Potatoes (red)");
		assertEquals("Potatoes (red)", product.getName());
	}

	@Test
	public void Gcpid_String_Is_Correct(){
		GenericProduct product = new GenericProduct(0, "Potatoes");
		assertEquals("0", product.getGcpidString());
	}
}
