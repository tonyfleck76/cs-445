package test.entities;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import lf2u.entities.Lf2u;
import lf2u.entities.customers.Customer;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.ViewFarmersResponse;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.exceptions.CustomerAccountNotFoundException;
import lf2u.exceptions.FarmerAccountNotFoundException;
import lf2u.exceptions.ManagerAccountNotFoundException;

public class Lf2uTest {

	private static String fid1, cid1;
	
	@BeforeClass
	public static void setup(){
		Lf2u lf2u = Lf2u.getInstace();
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		fid1 = lf2u.createNewFarmerAccount(new FarmInfo("Test 1", "Test Drive", "000-000-0000", "example.com"), 
									new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), 
									zips);
		cid1 = lf2u.createCustomerAccount(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), "Test Street", "60000");
	}
	
	@Test
	public void SuperUser_Account_Exists(){
		assertEquals("Super User", Lf2u.getInstace().findManagerAccountById(0).getName());
	}
	
	@Test(expected = ManagerAccountNotFoundException.class)
	public void FindManagerAccount_Fails_If_Account_Does_Not_Exist(){
		Lf2u.getInstace().findManagerAccountById(11);
	}
	
	@Test
	public void ManagerAccounts_Has_One_Account(){
		assertEquals(1, Lf2u.getInstace().getManagerAccounts().size());
	}
	
	@Test
	public void FarmerAccount_Can_Be_Updated(){
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60000");
		String fid = Lf2u.getInstace().createNewFarmerAccount(new FarmInfo("Test 1", "Test Drive", "000-000-0000", "example.com"), 
				new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), 
				zips);
		Lf2u.getInstace().updateFarmerAccount(Integer.parseInt(fid),
				new FarmInfo("Test 2", "Test Drive", "000-000-0000", "example.com"), 
				new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), 
				zips);
		assertEquals("Test 2", Lf2u.getInstace().findFarmerAccountById(Integer.parseInt(fid)).getFarmInfo().getName());
	}
	
	@Test(expected = FarmerAccountNotFoundException.class)
	public void UpdateFarmer_Fails_If_Cid_Does_Not_Exist(){
		Lf2u.getInstace().updateFarmerAccount(999, null, null, null);
	}
	
	@Test(expected = FarmerAccountNotFoundException.class)
	public void UpdateFarmer_Fails_If_Cid_Less_Than_Zero(){
		Lf2u.getInstace().updateFarmerAccount(-999, null, null, null);
	}
	
	@Test(expected = FarmerAccountNotFoundException.class)
	public void FindFarmerAccount_Fails_If_fid_Does_Not_Exist(){
		Lf2u.getInstace().findFarmerAccountById(999);
	}
	
	@Test(expected = FarmerAccountNotFoundException.class)
	public void FindFarmerAccount_Fails_If_fid_Less_Than_Zero(){
		Lf2u.getInstace().findFarmerAccountById(-1);
	}
	
	@Test
	public void CustomerAccount_Can_Be_Found(){
		Customer customer = Lf2u.getInstace().findCustomerAccountById(1);
		assertEquals("John Smith", customer.getName());
	}
	
	@Test(expected = CustomerAccountNotFoundException.class)
	public void FindCustomerAccount_Fails_If_Cid_Does_Not_Exist(){
		Lf2u.getInstace().findCustomerAccountById(999);
	}
	
	@Test(expected = CustomerAccountNotFoundException.class)
	public void FindCustomerAccount_Fails_If_Cid_Less_Than_Zero(){
		Lf2u.getInstace().findCustomerAccountById(-1);
	}
	
	@Test
	public void customer_Account_Can_Be_Updated(){
		String cid = Lf2u.getInstace().createCustomerAccount(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"), "Test Street", "60000");
		Lf2u.getInstace().updateCustomerAccount(Integer.parseInt(cid), 
				new ContactInfo("John Smith", "jsmith2@example.com", "000-000-0000"), 
				"Test Street", 
				"60000");
		assertEquals("jsmith2@example.com", Lf2u.getInstace().findCustomerAccountById(Integer.parseInt(cid)).getEmail());
	}
	
	@Test(expected = CustomerAccountNotFoundException.class)
	public void UpdateCustomer_Fails_If_Cid_Does_Not_Exist(){
		Lf2u.getInstace().updateCustomerAccount(999, null, null, null);
	}
	
	@Test(expected = CustomerAccountNotFoundException.class)
	public void UpdateCustomer_Fails_If_Cid_Less_Than_Zero(){
		Lf2u.getInstace().updateCustomerAccount(-999, null, null, null);
	}
}
