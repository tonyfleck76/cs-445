package test.entities.farmers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.farmers.ViewFarmersResponse;

public class ViewFarmersResponseTest {

	@Test
	public void Fid_Is_Set(){
		ViewFarmersResponse vfr = new ViewFarmersResponse("1", "Test Farm");
		assertEquals("1", vfr.getFid());
	}
	
	@Test
	public void Name_Is_Set(){
		ViewFarmersResponse vfr = new ViewFarmersResponse("1", "Test Farm");
		assertEquals("Test Farm", vfr.getName());
	}
}
