package test.entities.farmers;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.wrappers.ContactInfo;

public class FarmerTest {

	private static ArrayList<String> zipcodes;
	
	@BeforeClass
	public static void setup(){
		zipcodes = new ArrayList<String>();
		zipcodes.add("60000");
		zipcodes.add("00000");
	}
	
	@Test
	public void fid_Is_Correct(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(123, farm.getFid());
	}
	
	@Test
	public void DeliversTo_Is_Correct(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals("60000", farm.getDeliversTo().get(0));
	}
	
	
	/* Info empty when created */
	@Test
	public void FarmName_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getFarmInfo().getName());
	}
	
	@Test
	public void FarmAddress_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getFarmInfo().getAddress());
	}
	
	@Test
	public void FarmPhone_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getFarmInfo().getPhone());
	}
	
	@Test
	public void FarmWeb_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getFarmInfo().getWeb());
	}
	
	@Test
	public void Name_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getPersonalInfo().getName());
	}
	
	@Test
	public void Email_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getPersonalInfo().getEmail());
	}
	
	@Test
	public void Phone_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(null, farm.getPersonalInfo().getPhone());
	}
	
	/* Info Updates */
	@Test
	public void FarmName_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setFarmInfo(new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com"));
		assertEquals("Test Farm", farm.getFarmInfo().getName());
	}
	
	@Test
	public void FarmAddress_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setFarmInfo(new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com"));
		assertEquals("123 Example Dr", farm.getFarmInfo().getAddress());
	}
	
	@Test
	public void FarmPhone_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setFarmInfo(new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com"));
		assertEquals("000-000-0000", farm.getFarmInfo().getPhone());
	}
	
	@Test
	public void FarmWeb_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setFarmInfo(new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com"));
		assertEquals("www.testfarm.com", farm.getFarmInfo().getWeb());
	}
	
	@Test
	public void Name_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setPersonalInfo(new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000"));
		assertEquals("John Smith", farm.getPersonalInfo().getName());
	}
	
	@Test
	public void Email_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setPersonalInfo(new ContactInfo("John smith", "jsmith@example.com", "000-000-0000"));
		assertEquals("jsmith@example.com", farm.getPersonalInfo().getEmail());
	}
	
	@Test
	public void Phone_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setPersonalInfo(new ContactInfo("John smith", "jsmith@example.com", "000-000-0000"));
		assertEquals("000-000-0000", farm.getPersonalInfo().getPhone());
	}
	
	@Test
	public void deliversTo_Updates(){
		Farmer farm = new Farmer(123, zipcodes);
		ArrayList<String> zips = new ArrayList<String>();
		zips.add("60616");
		farm.setDeliversTo(zips);
		assertEquals("60616", farm.getDeliversTo().get(0));
	}
	
	@Test
	public void FidString_Is_Correct(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals("123", farm.getFidString());
	}
	
	@Test
	public void FarmInfo_Is_Correct(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setFarmInfo(new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com"));
		FarmInfo info = farm.getFarmInfo();
		assertEquals("Test Farm", info.getName());
	}
	
	@Test
	public void FarmStore_Is_Empty_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(0, farm.getFarmStore().getProducts().size());
	}
	
	@Test
	public void Delivery_Charge_Is_Zero_When_Created(){
		Farmer farm = new Farmer(123, zipcodes);
		assertEquals(0, farm.getDeliveryCharge(), 0);
	}
	
	@Test
	public void Delivery_Charge_Can_Be_Set(){
		Farmer farm = new Farmer(123, zipcodes);
		farm.setDeliveryCharge(2.0);
		assertEquals(2.0, farm.getDeliveryCharge(), 0);
	}
}
