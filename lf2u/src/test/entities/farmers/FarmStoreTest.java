package test.entities.farmers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lf2u.entities.farmers.FarmStore;
import lf2u.entities.farmers.ProductInfo;
import lf2u.exceptions.ProductNotFoundException;

public class FarmStoreTest {

	@Test
	public void Products_Is_Empty_When_Created(){
		FarmStore store = new FarmStore();
		assertEquals(0, store.getProducts().size());
	}
	
	@Test
	public void Add_Product_Works(){
		FarmStore store = new FarmStore();
		store.addProductToStore(new ProductInfo("Test", "Note", "10/01", "12/01", "lbs", "example.jps", 0.5));
		assertEquals("Test", store.getProductById(1).getName());
	}
	
	@Test(expected = ProductNotFoundException.class)
	public void GetProduct_Fails_If_FSPID_Does_Not_Exist(){
		FarmStore store = new FarmStore();
		store.getProductById(1);
	}
	
	@Test(expected = ProductNotFoundException.class)
	public void GetProduct_Fails_If_FSPID_Below_Zero(){
		FarmStore store = new FarmStore();
		store.getProductById(-1);
	}
}
