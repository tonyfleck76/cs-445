package test.entities.farmers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.farmers.FarmInfo;

public class FarmInfoTest {

	@Test
	public void Name_Is_Set_Correctly(){
		FarmInfo fi = new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com");
		assertEquals("Test Farm", fi.getName());
	}

	@Test
	public void Address_Is_Set_Correctly(){
		FarmInfo fi = new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com");
		assertEquals("123 Example Dr", fi.getAddress());
	}
	
	@Test
	public void Phone_Is_Set_Correctly(){
		FarmInfo fi = new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com");
		assertEquals("000-000-0000", fi.getPhone());
	}
	
	@Test
	public void Web_Is_Set_Correctly(){
		FarmInfo fi = new FarmInfo("Test Farm", "123 Example Dr", "000-000-0000", "www.testfarm.com");
		assertEquals("www.testfarm.com", fi.getWeb());
	}
}
