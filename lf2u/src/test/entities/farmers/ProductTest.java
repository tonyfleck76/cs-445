package test.entities.farmers;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import lf2u.entities.farmers.Product;
import lf2u.entities.farmers.ProductInfo;

public class ProductTest {
	
	private static ProductInfo info;
	
	@BeforeClass
	public static void setup(){
		info = new ProductInfo("Potatoes", "Test Note", "09-15", "10-15", "lbs", "test.jpg", 0.30);
	}
	
	@Test
	public void Fspid_Is_Correct(){
		Product product = new Product(0);
		assertEquals(0, product.getFspid());
	}
	
	@Test
	public void FspidString_Is_Correct(){
		Product product = new Product(0);
		assertEquals("0", product.getFspidString());
	}
	
	@Test
	public void Name_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals("Potatoes", product.getName());
	}

	@Test
	public void Note_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals("Test Note", product.getNote());
	}
	
	@Test
	public void Start_Date_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals("09-15", product.getStartDate());
	}
	
	@Test
	public void End_Date_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals("10-15", product.getEndDate());
	}
	
	@Test
	public void Product_Unit_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals("lbs", product.getProductUnit());
	}
	
	@Test
	public void Image_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals("test.jpg", product.getImage());
	}
	
	@Test
	public void Price_Is_Correct_After_Update(){
		Product product = new Product(0);
		product.setProductInfo(info);
		assertEquals(0.30, product.getPrice(), 0);
	}
	
	@Test
	public void Name_Fields_Updates(){
		Product product = new Product(0);
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("name", "Potatoes");
		product.updateProductFields(fields);
		assertEquals("Potatoes", product.getName());
	}
	
	@Test
	public void Note_Fields_Updates(){
		Product product = new Product(0);
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("note", "Delicious");
		product.updateProductFields(fields);
		assertEquals("Delicious", product.getNote());
	}
	
	@Test
	public void startDate_Fields_Updates(){
		Product product = new Product(0);
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("start_date", "10/01");
		product.updateProductFields(fields);
		assertEquals("10/01", product.getStartDate());
	}
	
	@Test
	public void EndDate_Fields_Updates(){
		Product product = new Product(0);
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("end_date", "10/01");
		product.updateProductFields(fields);
		assertEquals("10/01", product.getEndDate());
	}
	
	@Test
	public void Unit_Fields_Updates(){
		Product product = new Product(0);
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("product_unit", "lbs");
		product.updateProductFields(fields);
		assertEquals("lbs", product.getProductUnit());
	}
	
	@Test
	public void Price_Fields_Updates(){
		Product product = new Product(0);
		Map<String, Double> fields = new HashMap<String, Double>();
		fields.put("price", 1.5);
		product.updateProductFields(fields);
		assertEquals(1.5, product.getPrice(), 0);
	}
	
	@Test
	public void Image_Fields_Updates(){
		Product product = new Product(0);
		Map<String, String> fields = new HashMap<String, String>();
		fields.put("image", "example.jpg");
		product.updateProductFields(fields);
		assertEquals("example.jpg", product.getImage());
	}
}
