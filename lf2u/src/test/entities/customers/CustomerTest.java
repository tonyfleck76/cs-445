package test.entities.customers;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import lf2u.entities.customers.Customer;
import lf2u.entities.customers.Order;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.exceptions.OrderNotFoundException;

public class CustomerTest {

	private static ContactInfo info;
	
	@BeforeClass
	public static void setup(){
		info = new ContactInfo("John", "john@example.com", "000-000-0000");
	}
	
	@Test
	public void Cid_Is_Set_Correctly(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals(0, customer.getCid());
	}
	
	@Test
	public void Cid_String_Is_Correct(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals("0", customer.getCidString());
	}
	
	@Test
	public void Name_Is_Correct(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals("John", customer.getName());
	}
	
	@Test
	public void Email_Is_Correct(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals("john@example.com", customer.getEmail());
	}
	
	@Test
	public void Phone_Is_Correct(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals("000-000-0000", customer.getPhone());
	}
	
	@Test
	public void Street_Is_Correct(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals("1 fake drive", customer.getStreet());
	}
	
	@Test
	public void Zip_Is_Correct(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals("60000", customer.getZip());
	}
	
	@Test
	public void Info_Can_Be_Updated(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		customer.updateCustomerInfo(new ContactInfo("John", "john2@example.com", "000-000-0000"), "1 fake drive", "60000");
		assertEquals("john2@example.com", customer.getEmail());
	}
	
	@Test
	public void Add_Order_Returns_String(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		String oid = customer.addOrder(new Order(1, 1, "Note"));
		assertEquals("1", oid);
	}
	
	@Test
	public void Orders_Are_Returned_In_Array(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		String oid = customer.addOrder(new Order(1, 1, "Note"));
		assertEquals(1, customer.getOrders().size());
	}
	
	@Test
	public void Orders_Is_Empty_When_Created(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals(0, customer.getOrdersSize());
	}
	
	@Test
	public void Orders_Can_Be_Returned_In_Array(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		assertEquals(0, customer.getOrders().size());
	}
	
	@Test
	public void GetOrderById_Returns_Order(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		String oid = customer.addOrder(new Order(1, 1, "Note"));
		assertEquals(1, customer.getOrderById(1).getOid());
	}
	
	@Test(expected = OrderNotFoundException.class)
	public void GetOrderById_Fails_If_OID_Does_Not_Exist(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		customer.getOrderById(1);
	}
	
	@Test(expected = OrderNotFoundException.class)
	public void GetOrderById_Fails_If_OID_Less_Than_Zero(){
		Customer customer = new Customer(0, info, "1 fake drive", "60000");
		customer.getOrderById(-1);
	}
}
