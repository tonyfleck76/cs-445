package test.entities.customers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.customers.Order;
import lf2u.entities.farmers.FarmInfo;

public class OrderTest {

	@Test
	public void FarmInfo_Can_Be_Set(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		assertEquals("Test Farm", order.getFarmInfo().getName());
	}
	
	@Test
	public void OrderFarmInfo_Can_Be_Returned(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		assertEquals("1", order.getFarmInfo().getFidString());
	}
	
	@Test
	public void Totals_Can_Be_Calculated(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		order.createLineItem(0, 3, "Potatoes", 1.0, "lbs");
		order.calculateTotals(2);
		assertEquals(5.0, order.getOrderTotal(), 0);
	}
	
	@Test
	public void DeliveryCharge_Is_Set(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		order.createLineItem(0, 3, "Potatoes", 1.0, "lbs");
		order.calculateTotals(2);
		assertEquals(2.0, order.getDeliveryCharge(), 0);
	}
	
	@Test
	public void ProductTotal_Is_Correct(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		order.createLineItem(0, 3, "Potatoes", 1.0, "lbs");
		order.calculateTotals(2);
		assertEquals(3.0, order.getProductTotal(), 0);
	}
	
	@Test
	public void LineItems_Is_Empty_When_Created(){
		Order order = new Order(1, 1, "Note");
		assertEquals(0, order.getLineItems().size());
	}
	
	@Test
	public void Order_Can_Be_Canceled(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		order.createLineItem(0, 3, "Potatoes", 1.0, "lbs");
		order.calculateTotals(2);
		order.cancelOrder();
		assertEquals("canceled", order.getStatus());
	}
	
	@Test
	public void Delivery_Note_Is_Set(){
		Order order = new Order(1, 1, "Note");
		order.setFarmInfo(new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		order.createLineItem(0, 3, "Potatoes", 1.0, "lbs");
		order.calculateTotals(2);
		assertEquals("Note", order.getDeliveryNote());
	}
}
