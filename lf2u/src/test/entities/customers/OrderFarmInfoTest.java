package test.entities.customers;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.customers.OrderFarmInfo;
import lf2u.entities.farmers.FarmInfo;

public class OrderFarmInfoTest {

	@Test
	public void Address_Is_Correct(){
		OrderFarmInfo info = new OrderFarmInfo(1, new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		assertEquals("1 Farm Dr", info.getAddress());
	}
	
	@Test
	public void Phone_Is_Correct(){
		OrderFarmInfo info = new OrderFarmInfo(1, new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		assertEquals("000-000-0000", info.getPhone());
	}
	
	@Test
	public void Web_Is_Correct(){
		OrderFarmInfo info = new OrderFarmInfo(1, new FarmInfo("Test Farm", "1 Farm Dr", "000-000-0000", "testfarm.com"));
		assertEquals("testfarm.com", info.getWeb());
	}
}
