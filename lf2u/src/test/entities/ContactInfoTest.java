package test.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import lf2u.entities.wrappers.ContactInfo;

public class ContactInfoTest {

	@Test
	public void Name_Is_Set_Correctly(){
		ContactInfo pi = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		assertEquals("John Smith", pi.getName());
	}
	
	@Test
	public void Email_Is_Set_Correctly(){
		ContactInfo pi = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		assertEquals("jsmith@example.com", pi.getEmail());
	}
	
	@Test
	public void Phone_Is_Set_Correctly(){
		ContactInfo pi = new ContactInfo("John Smith", "jsmith@example.com", "000-000-0000");
		assertEquals("000-000-0000", pi.getPhone());
	}
}
