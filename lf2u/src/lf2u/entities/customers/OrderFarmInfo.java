package lf2u.entities.customers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lf2u.entities.farmers.FarmInfo;

@JsonPropertyOrder({"fid", "name", "address", "phone", "web"})
public class OrderFarmInfo {

	private int fid;
	private String name, address, phone, web;
	
	public OrderFarmInfo(int fid, FarmInfo farmInfo){
		this.fid = fid;
		this.name = farmInfo.getName();
		this.address = farmInfo.getAddress();
		this.phone = farmInfo.getPhone();
		this.web = farmInfo.getWeb();
	}

	@JsonProperty("fid")
	public String getFidString() {
		return String.valueOf(fid);
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	public String getWeb() {
		return web;
	}
	
}
