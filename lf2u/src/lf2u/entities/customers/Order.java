package lf2u.entities.customers;

import java.time.LocalDate;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.farmers.FarmInfo;

public class Order {

	private int oid, fid;
	private double productTotal, deliveryCharge, orderTotal;
	private ArrayList<OrderLineItem> lineItems;
	private String orderDate, plannedDeliveryDate, actualDeliveryDate, status, deliveryNote;
	private FarmInfo farmInfo;
	
	public Order(int oid, int fid, String deliveryNote){
		this.oid = oid;
		this.fid = fid;
		this.deliveryNote = deliveryNote;
		this.orderDate = getCurrentDateString();
		this.plannedDeliveryDate = getDeliveryDateString();
		this.status = "open";
		this.actualDeliveryDate = "";
		lineItems = new ArrayList<OrderLineItem>();
	}
	
	private String getCurrentDateString(){
		LocalDate date = LocalDate.now();
		return ""+date.getYear()+date.getMonthValue()+date.getDayOfMonth();
	}
	
	private String getDeliveryDateString(){
		LocalDate date = LocalDate.now();
		return ""+date.getYear()+date.getMonthValue()+(date.getDayOfMonth()+1);
	}
	
	public void createLineItem(int fspid, double amount, String name, double price, String unit){
		OrderLineItem lineItem = new OrderLineItem(fspid, amount);
		lineItem.setDetails(name, price, unit);
		lineItems.add(lineItem);
	}
	
	public void calculateTotals(double deliveryCharge){
		calculateLineItemTotal();
		this.deliveryCharge = deliveryCharge;
		this.orderTotal = this.productTotal+this.deliveryCharge;
	}
	
	private void calculateLineItemTotal(){
		double total = 0;
		for (OrderLineItem lineItem : lineItems){
			total += lineItem.getTotal();
		}
		this.productTotal = total;
	}
	
	public void setFarmInfo(FarmInfo farmInfo){
		this.farmInfo = farmInfo;
	}
	
	@JsonProperty("farm_info")
	public OrderFarmInfo getFarmInfo(){
		return new OrderFarmInfo(fid, farmInfo);
	}

	@JsonIgnore
	public int getOid() {
		return oid;
	}
	
	@JsonProperty("oid")
	public String getOidString(){
		return String.valueOf(oid);
	}

	@JsonIgnore
	public int getFid() {
		return fid;
	}
	
	@JsonProperty("products_total")
	public double getProductTotal() {
		return productTotal;
	}

	@JsonProperty("delivery_charge")
	public double getDeliveryCharge() {
		return deliveryCharge;
	}

	@JsonProperty("order_total")
	public double getOrderTotal() {
		return orderTotal;
	}

	@JsonProperty("order_detail")
	public ArrayList<OrderLineItem> getLineItems() {
		return new ArrayList<OrderLineItem>(lineItems);
	}

	@JsonProperty("order_date")
	public String getOrderDate() {
		return orderDate;
	}

	@JsonProperty("planned_delivery_date")
	public String getPlannedDeliveryDate() {
		return plannedDeliveryDate;
	}

	@JsonProperty("actual_delivery_date")
	public String getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public String getStatus() {
		return status;
	}
	
	public void cancelOrder(){
		this.status = "canceled";
	}

	@JsonProperty("delivery_note")
	public String getDeliveryNote() {
		return deliveryNote;
	}
}
