package lf2u.entities.customers;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lf2u.entities.wrappers.ContactInfo;
import lf2u.exceptions.OrderNotFoundException;

@JsonPropertyOrder({"cid", "name", "street", "zip", "phone", "email"})
public class Customer {
	
	private int cid;
	private String name, email, phone, zip, street;
	@JsonIgnore
	private ArrayList<Order> orders;
	
	public Customer(int cid, ContactInfo contactInfo, String street, String zip){
		this.cid = cid;
		this.name = contactInfo.getName();
		this.email = contactInfo.getEmail();
		this.phone = contactInfo.getPhone();
		this.zip = zip;
		this.street = street;
		this.orders= new ArrayList<Order>();
	}
	
	public void updateCustomerInfo(ContactInfo contactInfo, String street, String zip){
		this.name = contactInfo.getName();
		this.email = contactInfo.getEmail();
		this.phone = contactInfo.getPhone();
		this.zip = zip;
		this.street = street;
	}
	
	public String addOrder(Order order){
		orders.add(order);
		return order.getOidString();
	}
	
	@JsonIgnore
	public ArrayList<OrderInfo> getOrders(){
		ArrayList<OrderInfo> orderInfo = new ArrayList<OrderInfo>();
		for (Order order : orders){
			orderInfo.add(new OrderInfo(order));
		}
		return orderInfo;
	}
	
	@JsonIgnore
	public Order getOrderById(int id){
		if (id > orders.size() || id < 1){
			throw new OrderNotFoundException();
		}
		return orders.get(id-1);
	}
	
	@JsonIgnore
	public int getOrdersSize(){
		return orders.size();
	}

	@JsonIgnore
	public int getCid() {
		return cid;
	}
	
	@JsonProperty("cid")
	public String getCidString(){
		return String.valueOf(cid);
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getZip() {
		return zip;
	}

	public String getStreet() {
		return street;
	}
	
}
