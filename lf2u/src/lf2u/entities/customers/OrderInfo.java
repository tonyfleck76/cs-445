package lf2u.entities.customers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"oid", "order_date", "planned_delivery_date", "actual_delivery_date", "status", "fid"})
public class OrderInfo {

	private String oid, orderDate, plannedDeliveryDate, actualDeliveryDate, status, fid;
	
	public OrderInfo(Order order){
		this.oid = order.getOidString();
		this.orderDate = order.getOrderDate();
		this.plannedDeliveryDate = order.getPlannedDeliveryDate();
		this.actualDeliveryDate = order.getActualDeliveryDate();
		this.status = order.getStatus();
		this.fid = String.valueOf(order.getFid());
	}

	public String getOid() {
		return oid;
	}

	@JsonProperty("order_date")
	public String getOrderDate() {
		return orderDate;
	}

	@JsonProperty("planned_delivery_date")
	public String getPlannedDeliveryDate() {
		return plannedDeliveryDate;
	}

	@JsonProperty("actual_delivery_date")
	public String getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public String getStatus() {
		return status;
	}

	public String getFid() {
		return fid;
	}
	
}
