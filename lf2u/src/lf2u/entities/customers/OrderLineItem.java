package lf2u.entities.customers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"fspid", "name", "amount", "price", "line_item_total"})
public class OrderLineItem {

	int fspid;
	double amount, price, total;
	String name, unit;
	
	public OrderLineItem(int fspid, double amount){
		this.fspid = fspid;
		this.amount = amount;
	}
	
	public void setDetails(String name, double price, String unit){
		this.name = name;
		this.price = price;
		this.unit = unit;
		total = amount*price;
	}
	
	@JsonProperty("fspid")
	public String getFspidString(){
		return String.valueOf(fspid);
	}
	
	@JsonProperty("amount")
	public String getAmountString(){
		return amount+" "+unit;
	}
	
	@JsonProperty("price")
	public String getPriceString(){
		return price+" per "+unit;
	}
	
	public String getName(){
		return name;
	}
	
	@JsonProperty("line_item_total")
	public double getTotal(){
		return total;
	}
}
