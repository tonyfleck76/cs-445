package lf2u.entities.managers;

import java.util.ArrayList;

import lf2u.exceptions.GenericProductNotFoundException;

public class Catalog {

	private ArrayList<GenericProduct> genericProducts;
	
	public Catalog(){
		genericProducts = new ArrayList<GenericProduct>();
	}
	
	public String addGenericProduct(String productName){
		GenericProduct product = new GenericProduct(genericProducts.size()+1, productName);
		genericProducts.add(product);
		return product.getGcpidString();
	}
	
	public void updateGenericProduct(int id, String newName){
		if (id > genericProducts.size() || id < 0){
			throw new GenericProductNotFoundException();
		}
		genericProducts.get(id-1).setName(newName);
	}
	
	public ArrayList<GenericProduct> getGenericProducts(){
		return new ArrayList<GenericProduct>(genericProducts);
	}
}
