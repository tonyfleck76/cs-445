package lf2u.entities.managers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenericProduct {

	private int gcpid;
	private String name;
	
	public GenericProduct(int gcpid, String name){
		this.gcpid = gcpid;
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getGcpid(){
		return gcpid;
	}

	@JsonProperty("gcpid")
	public String getGcpidString() {
		return String.valueOf(gcpid);
	}

	public String getName() {
		return name;
	}
	
}
