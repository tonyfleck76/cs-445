package lf2u.entities.managers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lf2u.entities.wrappers.ContactInfo;

@JsonPropertyOrder({"mid", "name", "created_by", "create_date", "phone", "email"})
public class Manager {

	private int mid;
	private String name, createdBy, createDate, phone, email;
	
	public Manager(int mid, String createdBy, String createDate){
		this.mid = mid;
		this.createdBy = createdBy;
		this.createDate = createDate;
	}
	
	public void setContactInfo(ContactInfo info){
		this.name = info.getName();
		this.phone = info.getPhone();
		this.email = info.getEmail();
	}
	
	@JsonIgnore
	public int getMid(){
		return mid;
	}
	
	@JsonProperty("mid")
	public String getMidString() {
		return String.valueOf(mid);
	}

	public String getName() {
		return name;
	}

	@JsonProperty("created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	@JsonProperty("create_date")
	public String getCreateDate() {
		return createDate;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}
	
	
}
