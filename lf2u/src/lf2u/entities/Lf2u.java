package lf2u.entities;

import java.util.ArrayList;

import lf2u.entities.customers.Customer;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.managers.Catalog;
import lf2u.entities.managers.Manager;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.exceptions.CustomerAccountNotFoundException;
import lf2u.exceptions.FarmerAccountNotFoundException;
import lf2u.exceptions.ManagerAccountNotFoundException;

public class Lf2u {

	private static Lf2u instance;
	
	private ArrayList<Manager> managerAccounts;
	private ArrayList<Farmer> farmerAccounts;
	private ArrayList<Customer> customerAccounts;
	private Catalog catalog;
	
	private Lf2u(){
		// Defeat Instantiation
		
		managerAccounts = new ArrayList<Manager>();
		managerAccounts.add(new Manager(0, "System", "20161001"));
		managerAccounts.get(0).setContactInfo(new ContactInfo("Super User", "superuser@example.com", "123-654-0987"));
		
		farmerAccounts = new ArrayList<Farmer>();
		customerAccounts = new ArrayList<Customer>();
		
		catalog = new Catalog();
	}
	
	public static Lf2u getInstace(){
		if (instance == null){
			instance = new Lf2u();
		}
		return instance;
	}
	
	public Manager findManagerAccountById(int id){
		for (int index = 0; index < managerAccounts.size(); index++){
			if (managerAccounts.get(index).getMid() == id){
				return managerAccounts.get(index);
			}
		}
		throw new ManagerAccountNotFoundException();
	}
	
	public ArrayList<Manager> getManagerAccounts(){
		return new ArrayList<Manager>(managerAccounts);
	}
	
	public String createNewFarmerAccount(FarmInfo farmInfo, ContactInfo personalInfo, ArrayList<String> deliversTo){
		Farmer farmerAccount = new Farmer(farmerAccounts.size()+1, deliversTo);
		farmerAccount.setFarmInfo(farmInfo);
		farmerAccount.setPersonalInfo(personalInfo);
		farmerAccounts.add(farmerAccount);
		return farmerAccount.getFidString();
	}
	
	public void updateFarmerAccount(int id, FarmInfo farmInfo, ContactInfo personalInfo, ArrayList<String> deliversTo){
		if (id > farmerAccounts.size() || id < 1){
			throw new FarmerAccountNotFoundException();
		}
		Farmer farmerAccount = farmerAccounts.get(id-1);
		farmerAccount.setFarmInfo(farmInfo);
		farmerAccount.setPersonalInfo(personalInfo);
		farmerAccount.setDeliversTo(deliversTo);
	}
	
	public Farmer findFarmerAccountById(int id){
		if (id > farmerAccounts.size() || id < 1){
			throw new FarmerAccountNotFoundException();
		}
		return farmerAccounts.get(id-1);
	}
	
	public ArrayList<Farmer> getFarmerAccounts(){
		return new ArrayList<Farmer>(farmerAccounts);
	}
	
	public Customer findCustomerAccountById(int id){
		if (id > customerAccounts.size() || id < 1){
			throw new CustomerAccountNotFoundException();
		}
		return customerAccounts.get(id-1);
	}
	
	public String createCustomerAccount(ContactInfo info, String street, String zip){
		Customer customer = new Customer(customerAccounts.size()+1, info, street, zip);
		customerAccounts.add(customer);
		return customer.getCidString();
	}
	
	public void updateCustomerAccount(int id, ContactInfo info, String street, String zip){
		if (id > customerAccounts.size() || id < 1){
			throw new CustomerAccountNotFoundException();
		}
		Customer customerAccount = customerAccounts.get(id-1);
		customerAccount.updateCustomerInfo(info, street, zip);
	}
	
	public Catalog getCatalog(){
		return this.catalog;
	}
}
