package lf2u.entities.farmers;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

public class Product {

	@JsonPropertyOrder({"fspid", "name", "note", "start_date", "end_date", "price", "product_unit", "image"})
	
	private int fspid;
	private String name, note, startDate, endDate, productUnit, image;
	private double price;
	
	public Product(int fspid){
		this.fspid = fspid;
	}
	
	public void setProductInfo(ProductInfo info){
		this.name = info.getName();
		this.note = info.getNote();
		this.startDate = info.getStartDate();
		this.endDate = info.getEndDate();
		this.productUnit = info.getProductUnit();
		this.price = info.getPrice();
		this.image = info.getImage();
	}
	
	public void updateProductFields(Map<String, ?> fields){
		for (String key : fields.keySet()){
			updateField(fields, key);
		}
	}
	
	private void updateField(Map<String, ?> fields, String key){
		switch (key){
		case "name":
			this.name = (String) fields.get(key);
			break;
		case "note":
			this.note = (String) fields.get(key);
			break;
		case "start_date":
			this.startDate = (String) fields.get(key);
			break;
		case "end_date":
			this.endDate = (String) fields.get(key);
			break;
		case "product_unit":
			this.productUnit = (String) fields.get(key);
			break;
		case "price":
			this.price = (Double) fields.get(key);
			break;
		case "image":
			this.image = (String) fields.get(key);
			break;
		default:
			break;
		}					
	}
	
	@JsonIgnore
	public int getFspid(){
		return fspid;
	}

	@JsonProperty("fspid")
	public String getFspidString() {
		return String.valueOf(fspid);
	}

	public String getName() {
		return name;
	}

	public String getNote() {
		return note;
	}

	@JsonProperty("start_date")
	public String getStartDate() {
		return startDate;
	}

	@JsonProperty("end_date")
	public String getEndDate() {
		return endDate;
	}

	@JsonProperty("product_unit")
	public String getProductUnit() {
		return productUnit;
	}

	public String getImage() {
		return image;
	}

	public double getPrice() {
		return price;
	}
	
}
