package lf2u.entities.farmers;

import java.util.ArrayList;

import lf2u.exceptions.ProductNotFoundException;

public class FarmStore {

	private ArrayList<Product> products;
	
	public FarmStore(){
		products = new ArrayList<Product>();
	}
	
	public String addProductToStore(ProductInfo info){
		Product product = new Product(products.size()+1);
		product.setProductInfo(info);
		products.add(product);
		return product.getFspidString();
	}
	
	public Product getProductById(int fspid){
		if (fspid > products.size() || fspid < 1){
			throw new ProductNotFoundException();
		}
		return products.get(fspid - 1);
	}
	
	public ArrayList<Product> getProducts(){
		return new ArrayList<Product>(products);
	}
}
