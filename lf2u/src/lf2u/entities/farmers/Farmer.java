package lf2u.entities.farmers;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.wrappers.ContactInfo;

public class Farmer {

	private int fid;
	private String farmName, farmAddress, farmPhone, farmWeb, name, email, phone;
	private ArrayList<String> deliversTo;
	private FarmStore farmStore;
	private double deliveryCharge;
	
	public Farmer(int fid, ArrayList<String> deliversTo){
		this.fid = fid;
		this.deliversTo = deliversTo;
		farmStore = new FarmStore();
		deliveryCharge = 0;
	}
	
	public void setFarmInfo(FarmInfo info){
		this.farmName = info.getName();
		this.farmAddress = info.getAddress();
		this.farmPhone = info.getPhone();
		this.farmWeb = info.getWeb();
	}
	
	public void setPersonalInfo(ContactInfo info){
		this.name = info.getName();
		this.email = info.getEmail();
		this.phone = info.getPhone();
	}
	
	public void setDeliversTo(ArrayList<String> deliversTo){
		this.deliversTo = deliversTo;
	}

	@JsonIgnore
	public int getFid(){
		return this.fid;
	}
	
	@JsonProperty("fid")
	public String getFidString() {
		return String.valueOf(fid);
	}

	@JsonProperty("farm_info")
	public FarmInfo getFarmInfo(){
		return new FarmInfo(farmName, farmAddress, farmPhone, farmWeb);
	}

	@JsonProperty("personal_info")
	public ContactInfo getPersonalInfo(){
		return new ContactInfo(name, email, phone);
	}

	@JsonProperty("delivers_to")
	public ArrayList<String> getDeliversTo() {
		return deliversTo;
	}

	@JsonIgnore
	public FarmStore getFarmStore() {
		return farmStore;
	}
	
	@JsonIgnore
	public double getDeliveryCharge(){
		return deliveryCharge;
	}
	
	public void setDeliveryCharge(double deliveryCharge){
		this.deliveryCharge = deliveryCharge;
	}
}
