package lf2u.entities.farmers;

public class ProductInfo {

	private String name, note, startDate, endDate, productUnit, image;
	private double price;
	
	public ProductInfo(String name, String note, String startDate, String endDate, 
					   String productUnit, String image, double price){
		this.name = name;
		this.note = note;
		this.startDate = startDate;
		this.endDate = endDate;
		this.productUnit = productUnit;
		this.image = image;
		this.price = price;
	}
	
	public String getName(){
		return name;
	}
	
	public String getNote() {
		return note;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getProductUnit() {
		return productUnit;
	}

	public String getImage() {
		return image;
	}

	public double getPrice() {
		return price;
	}
}
