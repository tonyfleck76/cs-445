package lf2u.entities.farmers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FarmInfo {

	private String name, address, phone, web;
	
	@JsonCreator
	public FarmInfo(@JsonProperty("name") String name, @JsonProperty("address") String address, 
			   		@JsonProperty("phone") String phone, @JsonProperty("web") String web){
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.web = web;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	public String getWeb() {
		return web;
	}
	
	
}
