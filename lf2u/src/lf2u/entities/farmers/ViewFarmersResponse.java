package lf2u.entities.farmers;

public class ViewFarmersResponse {

	private String fid;
	private String name;
	
	public ViewFarmersResponse(String fid, String name){
		this.fid = fid;
		this.name = name;
	}

	public String getFid() {
		return fid;
	}

	public String getName() {
		return name;
	}
	
	
}
