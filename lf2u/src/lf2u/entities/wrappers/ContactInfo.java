package lf2u.entities.wrappers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactInfo {

	private String name, email, phone;
	
	@JsonCreator
	public ContactInfo(@JsonProperty("name") String name, @JsonProperty("email") String email, 
					   @JsonProperty("phone") String phone){
		this.name = name;
		this.email = email;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}
	
	
}
