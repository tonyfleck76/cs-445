package lf2u.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST	)
public class MissingCustomerInformationException extends RuntimeException {

}
