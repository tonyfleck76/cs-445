package lf2u.interactors.farmers;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmStore;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.Product;

public class GetFarmerProductDetailsTransaction {

	private int fid, fspid;
	
	public GetFarmerProductDetailsTransaction(int fid, int fspid){
		this.fid = fid;
		this.fspid = fspid;
	}
	
	public Product completeTransaction(){
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		FarmStore store = farmer.getFarmStore();
		return store.getProductById(fspid);
	}
}
