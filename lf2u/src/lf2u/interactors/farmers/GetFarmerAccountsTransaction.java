package lf2u.interactors.farmers;

import java.util.ArrayList;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.ViewFarmersResponse;

public class GetFarmerAccountsTransaction {
	
	public ArrayList<ViewFarmersResponse> completeTransaction(){
		ArrayList<ViewFarmersResponse> farmers = new ArrayList<ViewFarmersResponse>();
		
		for (Farmer farmer : Lf2u.getInstace().getFarmerAccounts()){
			farmers.add(new ViewFarmersResponse(farmer.getFidString(), farmer.getFarmInfo().getName()));
		}
		
		return farmers;
	}
	
	public ArrayList<ViewFarmersResponse> completeTransaction(String zipcode){
		ArrayList<ViewFarmersResponse> farmersForZip = new ArrayList<ViewFarmersResponse>();
		
		for (Farmer farmer : Lf2u.getInstace().getFarmerAccounts()){
			if (farmer.getDeliversTo().contains(zipcode)){
				farmersForZip.add(new ViewFarmersResponse(farmer.getFidString(), farmer.getFarmInfo().getName()));
			}
		}
		
		return farmersForZip;
	}
}
