package lf2u.interactors.farmers;

import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmStore;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.ProductInfo;
import lf2u.entities.managers.GenericProduct;
import lf2u.exceptions.GenericProductNotFoundException;

public class CreateFarmerProductTransaction {

	private String name, note, startDate, endDate, productUnit, image;
	private double price;
	
	public CreateFarmerProductTransaction(@JsonProperty("gcpid") int gcpid, @JsonProperty("note") String note,
										  @JsonProperty("start_date") String startDate, @JsonProperty("end_date") String endDate,
										  @JsonProperty("price") double price, @JsonProperty("product_unit") String productUnit,
										  @JsonProperty("image") String image){
		this.name = findGcpidName(gcpid);
		this.note = note;
		this.startDate = startDate;
		this.endDate = endDate;
		this.price = price;
		this.productUnit = productUnit;
		this.image = image;
	}
	
	public String completeTransaction(int fid){
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		FarmStore store = farmer.getFarmStore();
		ProductInfo info = new ProductInfo(name, note, startDate, endDate, productUnit, image, price);
		return store.addProductToStore(info);
	}
	
	private String findGcpidName(int gcpid){
		for (GenericProduct gp : Lf2u.getInstace().getCatalog().getGenericProducts()){
			if (gcpid == gp.getGcpid()){
				return gp.getName();
			}
		}
		throw new GenericProductNotFoundException();
	}
}
