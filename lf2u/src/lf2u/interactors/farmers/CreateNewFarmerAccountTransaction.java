package lf2u.interactors.farmers;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;

public class CreateNewFarmerAccountTransaction {

	private FarmInfo farmInfo;
	private ContactInfo personalInfo;
	private ArrayList<String> deliversTo;
	
	public CreateNewFarmerAccountTransaction(@JsonProperty("farm_info") FarmInfo farmInfo,
											 @JsonProperty("personal_info") ContactInfo personalInfo,
											 @JsonProperty("delivers_to") ArrayList<String> deliversTo){
		this.farmInfo = farmInfo;
		this.personalInfo = personalInfo;
		this.deliversTo = deliversTo;
	}
	
	public String completeTransaction(){
		return Lf2u.getInstace().createNewFarmerAccount(farmInfo, personalInfo, deliversTo);
	}
}

