package lf2u.interactors.farmers;

import java.util.Map;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmStore;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.Product;

public class UpdateFarmerProductTransaction {

	private Map<String, ?> fields;
	private int fid, fspid;
	
	public UpdateFarmerProductTransaction(int fid, int fspid, Map<String, ?> fields){
		this.fields = fields;
		this.fid = fid;
		this.fspid = fspid;
	}
	
	public void completeTransaction(){
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		FarmStore store = farmer.getFarmStore();
		Product product = store.getProductById(fspid);
		product.updateProductFields(fields);
	}
}
