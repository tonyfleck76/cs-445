package lf2u.interactors.farmers;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.FarmInfo;
import lf2u.entities.wrappers.ContactInfo;

public class UpdateFarmerAccountTransaction {

	private FarmInfo farmInfo;
	private ContactInfo personalInfo;
	private ArrayList<String> deliversTo;
	
	public UpdateFarmerAccountTransaction(@JsonProperty("farm_info") FarmInfo farmInfo,
											 @JsonProperty("personal_info") ContactInfo personalInfo,
											 @JsonProperty("delivers_to") ArrayList<String> deliversTo){
		this.farmInfo = farmInfo;
		this.personalInfo = personalInfo;
		this.deliversTo = deliversTo;
	}
	
	public void completeTransaction(int fid){
		Lf2u.getInstace().updateFarmerAccount(fid, farmInfo, personalInfo, deliversTo);
	}
}
