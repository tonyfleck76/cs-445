package lf2u.interactors.farmers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.Farmer;

public class UpdateFarmerDeliveryChargeTransaction {

	private double deliveryCharge;
	
	@JsonCreator
	public UpdateFarmerDeliveryChargeTransaction(@JsonProperty("delivery_charge") double deliveryCharge){
		this.deliveryCharge = deliveryCharge;
	}
	
	public void completeTransaction(int fid){
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		farmer.setDeliveryCharge(deliveryCharge);
	}
}
