package lf2u.interactors.farmers;

import java.util.ArrayList;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.Product;

public class GetFarmerProductsTransaction {

	private int fid;
	
	public GetFarmerProductsTransaction(int fid){
		this.fid = fid;
	}
	
	public ArrayList<Product> completeTransaction(){
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		return farmer.getFarmStore().getProducts();
	}
}
