package lf2u.interactors.farmers;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.Farmer;

public class GetFarmerDeliveryChargeTransaction {

	private int fid;
	
	public GetFarmerDeliveryChargeTransaction(int fid){
		this.fid = fid;
	}
	
	public double completeTransaction(){
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		return farmer.getDeliveryCharge();
	}
}
