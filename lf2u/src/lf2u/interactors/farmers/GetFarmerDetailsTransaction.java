package lf2u.interactors.farmers;

import lf2u.entities.Lf2u;
import lf2u.entities.farmers.Farmer;

public class GetFarmerDetailsTransaction {

	private int fid;
	
	public GetFarmerDetailsTransaction(int fid){
		this.fid = fid;
	}
	
	public Farmer completeTransaction(){
		return Lf2u.getInstace().findFarmerAccountById(fid);
	}
}
