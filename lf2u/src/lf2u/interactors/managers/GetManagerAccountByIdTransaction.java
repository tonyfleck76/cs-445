package lf2u.interactors.managers;

import lf2u.entities.Lf2u;
import lf2u.entities.managers.Manager;

public class GetManagerAccountByIdTransaction {

	private int id;
	
	public GetManagerAccountByIdTransaction(int id){
		this.id = id;
	}
	
	public Manager completeTransaction(){
		return Lf2u.getInstace().findManagerAccountById(id);
	}
}
