package lf2u.interactors.managers;

import java.util.ArrayList;

import lf2u.entities.Lf2u;
import lf2u.entities.managers.Manager;

public class GetManagerAccountsTransaction {
	
	public GetManagerAccountsTransaction(){

	}
	
	public ArrayList<Manager> completeTransaction(){
		return Lf2u.getInstace().getManagerAccounts();
	}
}
