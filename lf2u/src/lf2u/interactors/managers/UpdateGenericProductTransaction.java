package lf2u.interactors.managers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;

public class UpdateGenericProductTransaction {

	private String newName;
	
	@JsonCreator
	public UpdateGenericProductTransaction(@JsonProperty("name") String newName){
		this.newName = newName;
	}
	
	public void completeTransaction(int id){
		Lf2u.getInstace().getCatalog().updateGenericProduct(id, newName);
	}
}
