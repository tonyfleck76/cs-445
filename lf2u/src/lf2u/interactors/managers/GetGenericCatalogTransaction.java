package lf2u.interactors.managers;

import java.util.ArrayList;

import lf2u.entities.Lf2u;
import lf2u.entities.managers.GenericProduct;

public class GetGenericCatalogTransaction {

	public GetGenericCatalogTransaction(){
		
	}
	
	public ArrayList<GenericProduct> completeTransaction(){
		return Lf2u.getInstace().getCatalog().getGenericProducts();
	}
}
