package lf2u.interactors.managers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;

public class AddGenericProductTransaction {

	private String name;
	
	@JsonCreator
	public AddGenericProductTransaction(@JsonProperty("name") String name){
		this.name = name;
	}
	
	public String completeTransaction(){
		return Lf2u.getInstace().getCatalog().addGenericProduct(name);
	}
}
