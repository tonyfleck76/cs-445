package lf2u.interactors.customers;

import java.util.ArrayList;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.customers.Customer;
import lf2u.entities.customers.Order;
import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.Product;

public class CreateOrderForCustomerTransaction {

	private int fid;
	private ArrayList<Map<String, ?>> lineItems;
	private String deliveryNote;
	
	@JsonCreator
	public CreateOrderForCustomerTransaction(@JsonProperty("fid") int fid,
											 @JsonProperty("order_detail") ArrayList<Map<String, ?>> lineItems,
											 @JsonProperty("delivery_note") String deliveryNote){
		this.fid = fid;
		this.lineItems = lineItems;
		this.deliveryNote = deliveryNote;
	}
	
	public String completeTransaction(int cid){
		Customer customer = Lf2u.getInstace().findCustomerAccountById(cid);
		Farmer farmer = Lf2u.getInstace().findFarmerAccountById(fid);
		Order order = new Order(customer.getOrdersSize(), fid, deliveryNote);
		buildOrder(order, farmer);
		return customer.addOrder(order);
	}
	
	private void buildOrder(Order order, Farmer farmer){
		int fspid;
		double amount;
		Product product;
		order.setFarmInfo(farmer.getFarmInfo());
		for (Map<String, ?> lineItem : lineItems){
			fspid = Integer.parseInt((String) lineItem.get("fspid"));
			if (lineItem.get("amount").getClass() == Integer.class){
				amount = ((double)(int)(Integer)lineItem.get("amount"));
			} else {
				amount = (Double) lineItem.get("amount");
			}
			product = farmer.getFarmStore().getProductById(fspid);
			order.createLineItem(fspid, amount, product.getName(), product.getPrice(), product.getProductUnit());
		}
		order.calculateTotals(farmer.getDeliveryCharge());
	}
}
