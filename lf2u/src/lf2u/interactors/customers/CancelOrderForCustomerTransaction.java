package lf2u.interactors.customers;

import lf2u.entities.Lf2u;
import lf2u.entities.customers.Customer;
import lf2u.entities.customers.Order;

public class CancelOrderForCustomerTransaction {

	private int cid, oid;
	
	public CancelOrderForCustomerTransaction(int cid, int oid){
		this.cid = cid;
		this.oid = oid;
	}
	
	public void completeTransaction(){
		Customer customer = Lf2u.getInstace().findCustomerAccountById(cid);
		Order order = customer.getOrderById(oid);
		order.cancelOrder();
	}
}
