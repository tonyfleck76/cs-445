package lf2u.interactors.customers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.wrappers.ContactInfo;

public class CreateCustomerAccountTransaction {

	private ContactInfo info;
	private String street, zip;
	
	@JsonCreator
	public CreateCustomerAccountTransaction(@JsonProperty("name") String name,
											@JsonProperty("street") String street,
											@JsonProperty("zip") String zip,
											@JsonProperty("phone") String phone,
											@JsonProperty("email") String email){
		info = new ContactInfo(name, email, phone);
		this.street = street;
		this.zip = zip;
	}
	
	public String completeTransaction(){
		return Lf2u.getInstace().createCustomerAccount(info, street, zip);
	}
}
