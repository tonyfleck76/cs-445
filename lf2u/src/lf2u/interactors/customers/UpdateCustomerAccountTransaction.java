package lf2u.interactors.customers;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lf2u.entities.Lf2u;
import lf2u.entities.wrappers.ContactInfo;
import lf2u.exceptions.MissingCustomerInformationException;

public class UpdateCustomerAccountTransaction {

	private ContactInfo info;
	private String street, zip;
	
	@JsonCreator
	public UpdateCustomerAccountTransaction(@JsonProperty("name") String name,
											@JsonProperty("street") String street,
											@JsonProperty("zip") String zip,
											@JsonProperty("phone") String phone,
											@JsonProperty("email") String email){
		if (name == null || street == null || zip == null || phone == null || email == null){
			throw new MissingCustomerInformationException();
		}
		info = new ContactInfo(name, email, phone);
		this.street = street;
		this.zip = zip;
	}
	
	public void completeTransaction(int cid){
		Lf2u.getInstace().updateCustomerAccount(cid, info, street, zip);
	}
}
