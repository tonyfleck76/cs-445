package lf2u.interactors.customers;

import lf2u.entities.Lf2u;
import lf2u.entities.customers.Customer;

public class GetCustomerAccountTransaction {

	private int cid;
	
	public GetCustomerAccountTransaction(int cid){
		this.cid = cid;
	}
	
	public Customer completeTransaction(){
		return Lf2u.getInstace().findCustomerAccountById(cid);
	}
}
