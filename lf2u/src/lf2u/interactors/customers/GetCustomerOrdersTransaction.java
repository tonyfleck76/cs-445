package lf2u.interactors.customers;

import java.util.ArrayList;

import lf2u.entities.Lf2u;
import lf2u.entities.customers.Customer;
import lf2u.entities.customers.OrderInfo;

public class GetCustomerOrdersTransaction {

	private int cid;
	
	public GetCustomerOrdersTransaction(int cid){
		this.cid = cid;
	}
	
	public ArrayList<OrderInfo> completeTransaction(){
		Customer customer = Lf2u.getInstace().findCustomerAccountById(cid);
		return customer.getOrders();
	}
}
