package lf2u.boundaries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lf2u.entities.customers.Customer;
import lf2u.entities.customers.Order;
import lf2u.entities.customers.OrderInfo;
import lf2u.interactors.customers.CancelOrderForCustomerTransaction;
import lf2u.interactors.customers.CreateCustomerAccountTransaction;
import lf2u.interactors.customers.CreateOrderForCustomerTransaction;
import lf2u.interactors.customers.GetCustomerAccountTransaction;
import lf2u.interactors.customers.GetCustomerOrdersTransaction;
import lf2u.interactors.customers.GetOrderForCustomerTransaction;
import lf2u.interactors.customers.UpdateCustomerAccountTransaction;

@RestController
public class CustomerController {

	@RequestMapping(value = "/lf2u/customers", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Map<String, String> createCustomerAccount(@RequestBody CreateCustomerAccountTransaction transaction){
		String cid = transaction.completeTransaction();
		return Collections.singletonMap("cid", cid);
	}
	
	@RequestMapping(value = "/lf2u/customers/{cid}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Customer getCustomerAccount(@PathVariable int cid){
		GetCustomerAccountTransaction transaction = new GetCustomerAccountTransaction(cid);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/customers/{cid}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public void updateCustomerAccount(@PathVariable int cid,
									  @RequestBody UpdateCustomerAccountTransaction transaction){
		transaction.completeTransaction(cid);
	}
	
	@RequestMapping(value = "/lf2u/customers/{cid}/orders", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, String> createOrderForCustomer(@PathVariable("cid") int cid,
													  @RequestBody CreateOrderForCustomerTransaction transaction){
		String oid = transaction.completeTransaction(cid);
		return Collections.singletonMap("oid", oid);
	}
	
	@RequestMapping(value = "/lf2u/customers/{cid}/orders", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ArrayList<OrderInfo> getOrdersForCustomer(@PathVariable("cid") int cid){
		GetCustomerOrdersTransaction transaction = new GetCustomerOrdersTransaction(cid);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/customers/{cid}/orders/{oid}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Order getOrderForCustomer(@PathVariable("cid") int cid,
									 @PathVariable("oid") int oid){
		GetOrderForCustomerTransaction transaction = new GetOrderForCustomerTransaction(cid, oid);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/customers/{cid}/orders/{oid}", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ResponseBody
	public void cancelOrderForCustomer(@PathVariable("cid") int cid,
									 @PathVariable("oid") int oid){
		CancelOrderForCustomerTransaction transaction = new CancelOrderForCustomerTransaction(cid, oid);
		transaction.completeTransaction();
	}
}
