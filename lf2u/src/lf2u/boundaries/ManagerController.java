package lf2u.boundaries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lf2u.entities.managers.GenericProduct;
import lf2u.entities.managers.Manager;
import lf2u.interactors.managers.AddGenericProductTransaction;
import lf2u.interactors.managers.GetGenericCatalogTransaction;
import lf2u.interactors.managers.GetManagerAccountByIdTransaction;
import lf2u.interactors.managers.GetManagerAccountsTransaction;
import lf2u.interactors.managers.UpdateGenericProductTransaction;

@RestController
public class ManagerController {

	@RequestMapping(value = "/lf2u/managers/accounts", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ArrayList<Manager> getManagerAccounts(){
		GetManagerAccountsTransaction transaction = new GetManagerAccountsTransaction();
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/managers/accounts/{mid}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Manager getManagerAccountById(@PathVariable("mid") int id){
		GetManagerAccountByIdTransaction transaction = new GetManagerAccountByIdTransaction(id);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/managers/catalog", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ArrayList<GenericProduct> getCatalog(){
		GetGenericCatalogTransaction transaction = new GetGenericCatalogTransaction();
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/managers/catalog", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, String> addGenericProduct(@RequestBody AddGenericProductTransaction transaction){
		String gcpid = transaction.completeTransaction();
		return Collections.singletonMap("gcpid", gcpid);
	}
	
	@RequestMapping(value = "/lf2u/managers/catalog/{gcpid}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateGenericProduct(@PathVariable int gcpid, @RequestBody UpdateGenericProductTransaction transaction){
		transaction.completeTransaction(gcpid);
	}
}
