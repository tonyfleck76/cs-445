package lf2u.boundaries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lf2u.entities.farmers.Farmer;
import lf2u.entities.farmers.Product;
import lf2u.entities.farmers.ViewFarmersResponse;
import lf2u.interactors.farmers.CreateFarmerProductTransaction;
import lf2u.interactors.farmers.CreateNewFarmerAccountTransaction;
import lf2u.interactors.farmers.GetFarmerAccountsTransaction;
import lf2u.interactors.farmers.GetFarmerDeliveryChargeTransaction;
import lf2u.interactors.farmers.GetFarmerDetailsTransaction;
import lf2u.interactors.farmers.GetFarmerProductDetailsTransaction;
import lf2u.interactors.farmers.GetFarmerProductsTransaction;
import lf2u.interactors.farmers.UpdateFarmerAccountTransaction;
import lf2u.interactors.farmers.UpdateFarmerDeliveryChargeTransaction;
import lf2u.interactors.farmers.UpdateFarmerProductTransaction;

@RestController
public class FarmerController {

	@RequestMapping(value = "/lf2u/farmers", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public ArrayList<ViewFarmersResponse> getFarmerAccounts(@RequestParam(value = "zip", required = false) String zipcode){
		GetFarmerAccountsTransaction transaction = new GetFarmerAccountsTransaction();
		if (zipcode == null) return transaction.completeTransaction();
		else return transaction.completeTransaction(zipcode);
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Farmer GetFarmerAccount(@PathVariable("fid") int fid){
		GetFarmerDetailsTransaction transaction = new GetFarmerDetailsTransaction(fid);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/farmers", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, String> createFarmerAccount(@RequestBody CreateNewFarmerAccountTransaction transaction){
		String fid = transaction.completeTransaction();
		return Collections.singletonMap("fid", fid);
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public void updateFarmerAccount(@PathVariable("fid") int fid, 
									@RequestBody UpdateFarmerAccountTransaction transaction){
		transaction.completeTransaction(fid);
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}/products", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public ArrayList<Product> getFarmerProducts(@PathVariable("fid") int fid){
		GetFarmerProductsTransaction transaction = new GetFarmerProductsTransaction(fid);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}/products", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Map<String, String> createFarmerProduct(@PathVariable("fid") int fid,
												   @RequestBody CreateFarmerProductTransaction transaction){
		String fspid = transaction.completeTransaction(fid);
		return Collections.singletonMap("fspid", fspid);
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}/products/{fspid}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Product getFarmerProductById(@PathVariable("fid") int fid,
										@PathVariable("fspid") int fspid){
		GetFarmerProductDetailsTransaction transaction = new GetFarmerProductDetailsTransaction(fid, fspid);
		return transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}/products/{fspid}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void updateFarmerProduct(@PathVariable("fid") int fid,
									@PathVariable("fspid") int fspid,
									@RequestBody Map<String, ?> fields){
		UpdateFarmerProductTransaction transaction = new UpdateFarmerProductTransaction(fid, fspid, fields);
		transaction.completeTransaction();
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}/delivery_charge", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Map<String, Double> getFarmerDeliveryCharge(@PathVariable("fid") int fid){
		GetFarmerDeliveryChargeTransaction transaction = new GetFarmerDeliveryChargeTransaction(fid);
		double charge = transaction.completeTransaction();
		return Collections.singletonMap("delivery_charge", charge);
	}
	
	@RequestMapping(value = "/lf2u/farmers/{fid}/delivery_charge", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateFarmerDeliveryCharge(@PathVariable("fid") int fid,
										   @RequestBody UpdateFarmerDeliveryChargeTransaction transaction){
		transaction.completeTransaction(fid);
		ResponseEntity<String> response = new ResponseEntity<String>(HttpStatus.NO_CONTENT);
		return response;
	}
}
